<?php
/**
 * @author: pawelbujakowski
 */

namespace Host;

use Request\Request;

interface Host
{
    /**
     * @return float
     */
    public function getLoad(): float;

    /**
     * @param Request $request
     */
    public function handleRequest(Request $request): void;
}