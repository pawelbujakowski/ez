<?php
/**
 * @author: pawelbujakowski
 */

namespace Host;

class HostsCollection implements \IteratorAggregate, \Countable, \ArrayAccess
{
    /**
     * @var Host[]
     */
    private $hosts = [];

    /**
     * HostsCollection constructor.
     * @param Host[] $hosts
     */
    public function __construct(array $hosts)
    {
        if (count($hosts) == 0) {
            throw new \InvalidArgumentException('Hosts collection can not be empty');
        }

        foreach ($hosts as $host) {
            if (!$host instanceof Host) {
                throw new \InvalidArgumentException(
                    sprintf('Hosts collection can only contain %s objects', Host::class)
                );
            }
        }

        $this->hosts = array_values($hosts);
    }

    /**
     * @inheritdoc
     */
    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->hosts);
    }

    /**
     * @inheritdoc
     */
    public function count(): int
    {
        return count($this->hosts);
    }

    /**
     * @inheritdoc
     */
    public function offsetExists($offset): bool
    {
        return isset($this->hosts[(int)$offset]);
    }

    /**
     * @inheritdoc
     */
    public function offsetGet($offset)
    {
        return isset($this->hosts[(int)$offset]) ? $this->hosts[(int)$offset] : null;
    }

    /**
     * @inheritdoc
     */
    public function offsetSet($offset, $host)
    {
        if (!$host instanceof Host) {
            throw new \InvalidArgumentException('Hosts collection can only contain Host/Host objects');
        }

        if (!is_int($offset)) {
            throw new \InvalidArgumentException('Offset need to be a integer');
        }

        $this->hosts[(int)$offset] = $host;
    }

    /**
     * @inheritdoc
     */
    public function offsetUnset($offset)
    {
        if (isset($this->hosts[(int)$offset])) {

            unset($this->hosts[(int)$offset]);
            $this->hosts = array_values($this->hosts);
        }
    }
}