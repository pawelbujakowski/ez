<?php
/**
 * @author: pawelbujakowski
 */

namespace LoadBalancer;

use Host\HostsCollection;
use LoadBalancer\BalancingStrategy\BalancingStrategy;
use Request\Request;

class LoadBalancer
{
    /**
     * @var HostsCollection
     */
    private $hosts;

    /**
     * @var BalancingStrategy
     */
    private $balancingStrategy;

    /**
     * @param HostsCollection $hosts
     * @param BalancingStrategy $balancingStrategy
     */
    public function __construct(HostsCollection $hosts, BalancingStrategy $balancingStrategy)
    {
        $this->hosts = $hosts;
        $this->balancingStrategy = $balancingStrategy;
    }

    /**
     * @param Request $request
     */
    public function handleRequest(Request $request):void
    {
        $host = $this->balancingStrategy->chooseHost($this->hosts);
        $host->handleRequest($request);
    }
}
