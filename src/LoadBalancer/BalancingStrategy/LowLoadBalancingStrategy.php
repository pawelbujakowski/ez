<?php
/**
 * @author: pawelbujakowski
 */

namespace LoadBalancer\BalancingStrategy;

use Host\Host;
use Host\HostsCollection;

class LowLoadBalancingStrategy implements BalancingStrategy
{
    /**
     * @inheritdoc
     */
    public function chooseHost(HostsCollection $hosts): Host
    {
        /** @var Host $lowestLoad */
        $lowestLoad = null;

        /** @var Host $host */
        foreach ($hosts as $host) {

            if($host->getLoad() < 0.75) {
                return $host;
            }

            if(empty($lowestLoad) || $lowestLoad->getLoad() > $host->getLoad()) {
                $lowestLoad = $host;
            }
        }

        return $lowestLoad;
    }
}
