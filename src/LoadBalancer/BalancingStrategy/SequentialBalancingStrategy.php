<?php
/**
 * @author: pawelbujakowski
 */

namespace LoadBalancer\BalancingStrategy;

use Host\Host;
use Host\HostsCollection;

class SequentialBalancingStrategy implements BalancingStrategy
{
    /**
     * @var HostsCollection
     */
    private $hosts;

    /**
     * @var int
     */
    private $currentPosition = 0;

    /**
     * @param HostsCollection $hosts
     * @return Host
     */
    public function chooseHost(HostsCollection $hosts): Host
    {
        if(!$this->hosts) {
            $this->hosts = $hosts;
        }

        if($this->hosts !== $hosts) {
            throw new \InvalidArgumentException('Strategy already used with different collection of hosts');
        }

        $index = $this->getNextIndex();

        return $this->hosts[$index];
    }

    /**
     * @return int
     */
    private function getNextIndex(): int
    {
        $index = $this->currentPosition % count($this->hosts);
        $this->currentPosition++;

        return $index;
    }
}
