<?php
/**
 * @author: pawelbujakowski
 */

namespace LoadBalancer\BalancingStrategy;

use Host\Host;
use Host\HostsCollection;

interface BalancingStrategy
{
    /**
     * @param HostsCollection $hosts
     * @return Host
     */
    public function chooseHost(HostsCollection $hosts): Host;
}