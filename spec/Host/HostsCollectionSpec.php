<?php
/**
 * @author: pawelbujakowski
 */

namespace spec\Host;

use Host\Host;
use Host\HostsCollection;
use PhpSpec\ObjectBehavior;

class HostsCollectionSpec extends ObjectBehavior
{
    function it_is_initializable(Host $host1, Host $host2)
    {
        $this->beConstructedWith([$host1,$host2]);
        $this->shouldHaveType(HostsCollection::class);
    }

    function it_is_constructed_with_array_of_hosts(Host $host1, Host $host2)
    {
        $this->beConstructedWith([$host1,$host2]);
    }

    function it_throw_exception_if_is_constructed_with_different_objects_then_host(Host $host1, Host $host2)
    {
        $differentObject = new \stdClass();
        $this->beConstructedWith([$host1,$differentObject, $host2]);
        $this->shouldThrow('InvalidArgumentException')->duringInstantiation();
    }

    function it_throw_exception_if_is_constructed_with_empty_array()
    {
        $this->beConstructedWith([]);
        $this->shouldThrow('InvalidArgumentException')->duringInstantiation();
    }

    function it_is_countable(Host $host1, Host $host2)
    {
        $this->beConstructedWith([$host1,$host2]);
        $this->shouldHaveCount(2);
    }

    function it_is_iterable(Host $host1, Host $host2)
    {
        $hosts = [$host1, $host2];

        $this->beConstructedWith($hosts);

        $this->shouldHaveKeyWithValue(0,$host1);
        $this->shouldHaveKeyWithValue(1,$host2);
    }
}
