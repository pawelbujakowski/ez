<?php
/**
 * @author: pawelbujakowski
 */

namespace spec\LoadBalancer\BalancingStrategy;

use Host\Host;
use Host\HostsCollection;
use LoadBalancer\BalancingStrategy\BalancingStrategy;
use LoadBalancer\BalancingStrategy\LowLoadBalancingStrategy;
use PhpSpec\ObjectBehavior;

class LowLoadBalancingStrategySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(LowLoadBalancingStrategy::class);
        $this->shouldImplement(BalancingStrategy::class);
    }

    function it_return_first_host_under_075(Host $host1, Host $host2, Host $host3, Host $host4)
    {
        $host1->getLoad()->willreturn(0.8);
        $host2->getLoad()->willreturn(0.7);
        $host3->getLoad()->willreturn(1.2);
        $host4->getLoad()->willreturn(0.2);

        $hosts = new HostsCollection([
            $host1->getWrappedObject(),
            $host2->getWrappedObject(),
            $host3->getWrappedObject(),
            $host4->getWrappedObject()
        ]);

        $this->chooseHost($hosts)->shouldReturn($host2);
    }

    function it_return_lowest_if_all_above_075(Host $host1, Host $host2, Host $host3, Host $host4)
    {
        $host1->getLoad()->willreturn(0.8);
        $host2->getLoad()->willreturn(0.9);
        $host3->getLoad()->willreturn(1.2);
        $host4->getLoad()->willreturn(0.76);

        $hosts = new HostsCollection([
            $host1->getWrappedObject(),
            $host2->getWrappedObject(),
            $host3->getWrappedObject(),
            $host4->getWrappedObject()
        ]);

        $this->chooseHost($hosts)->shouldReturn($host4);
    }


}
