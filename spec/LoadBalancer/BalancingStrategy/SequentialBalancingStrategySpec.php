<?php
/**
 * @author: pawelbujakowski
 */

namespace spec\LoadBalancer\BalancingStrategy;

use Host\Host;
use Host\HostsCollection;
use LoadBalancer\BalancingStrategy\BalancingStrategy;
use LoadBalancer\BalancingStrategy\SequentialBalancingStrategy;
use PhpSpec\ObjectBehavior;

class SequentialBalancingStrategySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(SequentialBalancingStrategy::class);
        $this->shouldImplement(BalancingStrategy::class);
    }

    function it_should_start_from_first_host(Host $host1, Host $host2)
    {
        $hosts = new HostsCollection([$host1->getWrappedObject(), $host2->getWrappedObject()]);

        $this->chooseHost($hosts)->shouldReturn($host1);
    }

    function it_should_iterate_over_list_of_hosts(Host $host1, Host $host2)
    {
        $hosts = new HostsCollection([$host1->getWrappedObject(), $host2->getWrappedObject()]);

        $this->chooseHost($hosts);
        $this->chooseHost($hosts)->shouldReturn($host2);
    }

    function it_should_wrap_list_of_hosts(Host $host1, Host $host2)
    {
        $hosts = new HostsCollection([$host1->getWrappedObject(), $host2->getWrappedObject()]);

        $this->chooseHost($hosts);
        $this->chooseHost($hosts);
        $this->chooseHost($hosts)->shouldReturn($host1);
    }
}