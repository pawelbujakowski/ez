<?php
/**
 * @author: pawelbujakowski
 */

namespace spec\LoadBalancer;

use Host\Host;
use Host\HostsCollection;
use LoadBalancer\BalancingStrategy\BalancingStrategy;
use LoadBalancer\LoadBalancer;
use PhpSpec\ObjectBehavior;
use Request\Request;

class LoadBalancerSpec extends ObjectBehavior
{
    function it_is_constructed_with_hosts_collection_and_balancing_strategy(HostsCollection $hostsCollection, BalancingStrategy $strategy)
    {
        $this->beConstructedWith($hostsCollection, $strategy);
        $this->shouldHaveType(LoadBalancer::class);
    }

    function it_should_call_handle_Request_on_selected_host(Host $host, Request $request, HostsCollection $hostsCollection, BalancingStrategy $balancingStrategy)
    {
        $balancingStrategy->chooseHost($hostsCollection)->willReturn($host);
        $this->beConstructedWith($hostsCollection, $balancingStrategy);

        $this->handleRequest($request);
        $host->handleRequest($request)->shouldHaveBeenCalled();
    }
}
